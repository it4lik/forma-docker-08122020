# Déroulement formation Docker

<!-- vim-markdown-toc GitLab -->

* [I. Installation Docker CentOS8](#i-installation-docker-centos8)
* [II. Configuration de Docker](#ii-configuration-de-docker)
* [III. Commandes élémentaires de Docker](#iii-commandes-élémentaires-de-docker)
    * [1. Récupérer une image depuis le Docker Hub](#1-récupérer-une-image-depuis-le-docker-hub)
    * [2. Lancer un conteneur, à partir d'une image donnée](#2-lancer-un-conteneur-à-partir-dune-image-donnée)
    * [3. Récupérer un terminal dans un conteneur existant](#3-récupérer-un-terminal-dans-un-conteneur-existant)
* [IV. Création d'une première image Docker](#iv-création-dune-première-image-docker)
    * [1. Rédaction du Dockerfile](#1-rédaction-du-dockerfile)
    * [2. Construction de l'image](#2-construction-de-limage)
    * [3. Test du bon fonctionnement](#3-test-du-bon-fonctionnement)
* [V. Rédaction de Dockerfiles](#v-rédaction-de-dockerfiles)
    * [1. Dockerfile Apache](#1-dockerfile-apache)
        * [A. Lancement simple](#a-lancement-simple)
        * [B. Partage de ports et volume](#b-partage-de-ports-et-volume)
    * [2. Dockerfile MariaDB](#2-dockerfile-mariadb)
* [VI. `docker-compose`](#vi-docker-compose)
    * [1. Premiers pas](#1-premiers-pas)
    * [2. Volumes et Networks](#2-volumes-et-networks)
    * [3. Wordpress + Reverse proxy](#3-wordpress-reverse-proxy)

<!-- vim-markdown-toc -->

# I. Installation Docker CentOS8

Suivre la documentation officielle spécifique à notre OS : https://docs.docker.com/engine/install/centos/

```bash
$ yum install yum-utils

# Ajout des dépôts Docker
$ yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
    
# Installation de Docker
$ yum install docker-ce  docker-ce-cli containerd.io

# Démarrage de Docker
$ systemctl start docker
$ systemctl enable docker
```

# II. Configuration de Docker

```bash
# Devenir root
$ su - 

# Ajouter l'utilisateur de session au groupe docker
$ usermod -aG docker <USERNAME>
```

# III. Commandes élémentaires de Docker

## 1. Récupérer une image depuis le [Docker Hub](https://hub.docker.com/)

```bash
$ docker pull 
# Par exemple
$ docker pull debian
```

> Par défaut, les images sont récupérées depuis le [Docker Hub](https://hub.docker.com/). Pour des images très utilisées comme [l'image Debian](https://hub.docker.com/_/debian), elles sont hébergées dans un dépôt appelé "library" : ce sont les images officielles, dignes de confiance.

## 2. Lancer un conteneur, à partir d'une image donnée

```bash
# Lance un conteneur basé sur l'image Debian
$ docker run debian

# Lance un conteneur basé sur l'image Debian qui exécute un processus sleep
# L'option "-d" est pour "daemon" : le conteneur s'exécutera en tâche de fond
$ docker run -d debian sleep 10000
```

> Si l'image n'est pas présente sur la machine, `docker` essaiera de la télécharger automatiquement depuis le Docker Hub.

## 3. Récupérer un terminal dans un conteneur existant 

```bash
# On repère le nom ou l'ID d'un conteneur lancé
$ docker ps

# On exécute un terminal bash dans le conteneur, en mode interactif
# Les options -it permettent la session interactive
$ docker exec -it <NOM_OU_ID_CONTENEUR> bash
```

> **NB :** il faut que le terminal `bash` soit présent dans le conteneur.

# IV. Création d'une première image Docker

Pour construire une image, il faut la générer (il faut la "build") depuis un fichier appelé `Dockerfile`. Le `Dockerfile` contient les instructions nécessaires à construire l'image.

On utilise la commande `docker build` pour générer une image à partir d'un `Dockerfile`

Une image pourra par la suite être instanciée de multiple fois pour créer des conteneurs.

Une image est strictement immuable : on ne peut la modifier. Pour mettre à jour une image, il est nécessaire de mettre à jour le `Dockerfile` et relance un `docker build`.

## 1. Rédaction du Dockerfile

```bash
# Création d'un fichier Dockerfile simpliste
$ vim Dockerfile

$ cat Dockerfile
FROM debian
RUN touch /opt/dockerforma
```

> La **[documentation officielle](https://docs.docker.com/engine/reference/builder/)** est très claire et complète à ce sujet.

## 2. Construction de l'image

```bash
# A partir du Dockerfile, on génère une image
$ docker build -t forma-debian:forma .
```

L'option `-t` (pour "tag") permet de donner un nom et une version à l'image construite.

Le `.` en fin de ligne indique le "contexte de build" : c'est le dossier à partir duquel sera lancé le build (tout ce qui est nécessaire au build se trouve donc dans ce dossier : fichiers de configurations, etc).

## 3. Test du bon fonctionnement

```bash
# A partir de l'image que l'on vient de construire, on instancie un conteneur
$ docker run -d forma-debian:forma

# On repère le nom ou l'ID du conteneur lancé
$ docker ps

# Exécution d'un terminal bash interactif à l'intérieur du conteneur
$ docker exec -it <NOM_OU_ID_CONTENEUR> bash

# Nous sommes désormais à l'intérieur du conteneur
# On peut donc vérifier l'existence du fichier créé dans l'image lors du build : /opt/dockerforma
$ ls /opt
dockerforma
```

# V. Rédaction de Dockerfiles

## 1. Dockerfile Apache

### A. Lancement simple

```Dockerfile
# Utilisation d'une image de base debian
# Implicitement, c'est la version "latest" qui sera récupérée
FROM debian

# Mise à jour des dépôts du système
RUN apt-get update -y

# Installation de Apache
RUN apt-get install -y apache2

# On ajoute un utilisateur "web" dans le conteneur
RUN useradd web

# Configuration de Apache
COPY apache2.conf /etc/apache2/apache2.conf

# On indique que tout conteneur issu de cette image devra lancer Apache
CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]
```

Fichier `apache2.conf` associé : 
```conf
DefaultRuntimeDir /opt
PidFile /opt/apache.pid
Timeout 300
KeepAlive On
MaxKeepAliveRequests 100
KeepAliveTimeout 5
User web
Group web
HostnameLookups Off
ErrorLog /opt/error.log
LogLevel warn
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf
Include ports.conf
IncludeOptional sites-enabled/*.conf
```

Suite d'opérations pour tester : 
```bash
# On construit l'image
$ docker build -t apache:forme

# On lance l'image
$ docker run -d apache:forma

# L'image démarre mais s'arrête immédiatement
$ docker ps
$ docker ps -a

# On regarde les logs du conteneur
$ docker logs <NOM_OU_ID_CONTENEUR>

# Une variable d'environnement APACHE_LOG_DIR n'est pas définie. On peut la définir au lancement du conteneur
$ docker run -d -e APACHE_LOG_DIR=/opt apache:forma

# Pour tester le bon fonctionnement du serveur Web, il faut connaître l'IP du conteneur
$ docker inspect <NOM_OU_ID_CONTENEUR>
$ docker inspect <NOM_OU_ID_CONTENEUR> | grep -i ipa # récupère directement la ligne avec l'IP

# A l'aide du terminal, on teste avec une requête HTTP le bon fonctionnement su serveur web
$ curl <IP_CONTENEUR>

# On peut aussi visiter l'IP du conteneur avec un navigateur web
```

### B. Partage de ports et volume

On réutilise l'image `apache:forma` qui a été construite ci-dessus, mais en ajoutant : 
* un partage de ports
  * option `-p` de `docker run`
  * permet d'accéder au service du conteneur en utilisant l'IP de l'hôte directement
  * ainsi, les machines pouvant joindre l'hôte seront en mesure d'accéder au service
* un volume Docker partagé entre l'hôte et le conteneur
  * option `-v` de `docker run`
  * synchronise un dossier entre l'hôte et le conteneur
  * ici, on déposera un nouveau `index.html` de notre serveur web

```bash
# On repère le dossier où l'on est afin d'utiliser des chemins absolus
$ pwd
/root/web

# Création du fichier index.html
$ echo "<h1>Hello forma</h1>" > index.html

# Lancement du conteneur
# Pour que la commande soit plus lisible, on peut l'éclater sur plusieurs lignes avec des \ à la fin de chaque ligne
$ docker run -d \
    -e APACHE_LOG_DIR=/opt apache:forma \
    -v /root/web/index.html:/var/www/html/index.html \
    -p 8888:80 \
    apache:forma

# Test du bon fonctionnement
$ curl localhost
<h1>Hello forma</h1>
# On peut là aussi tester avec un navigateur en visitant l'URL http://localhost
```

## 2. Dockerfile MariaDB

Rédaction du `Dockerfile` :
```bash
$ vim Dockerfile

$ cat Dockerfile
FROM debian:10
  
RUN apt-get update -y

RUN apt-get install -y mariadb-server

CMD ["mysqld_safe"]
```

Suite d'opérations pour tester : 
```bash
# On construit l'image
$ docker build -t mysql:forma

# On lance l'image
# On partage le port 3306 de l'adresse de 127.0.0.1 de l'hôte
# Vers le port 3306 du conteneur (le port utilisé par MySQL par défaut)
$ docker run -d -p 127.0.0.1:3306:3306 mysql:forma

# L'image démarre correctement
$ docker ps

# On vérifie que l'application tourne sur le port 3306
# On peut essayer de se connecter à l'aide d'une commande mysql
# Ou simplement vérifier que notre port est ouvert sur l'hôte :
$ ss -alntp | grep 3306
```

# VI. `docker-compose`

## 1. Premiers pas

`docker-compose` est un utilitaire additionel fournir par Docker. Il permet plusieurs choses :
* transformer des lignes de commande `docker run` en fichier `docker-compose.yml`, bien plus faciles à maintenir
* permettre le **lancement simultané de plusieurs conteneurs**
* ajoute des fonctionnalités, principalement utilisées pour faire fonctionner plusieurs conteneurs de concert
  * relations de dépendance entre les conteneurs
  * volumes partagés entre les conteneurs
  * variables d'environnement partagées
  * etc.

Fichier `docker-compose.yml` d'exemple qui lance deux conteneurs (notre conteneur HTTPD `apache:forma` et le conteneur MySQL officiel) : 

```yaml
version: "3.8"

services:
  web:
    image: apache:forma
    environment:
      - "APACHE_LOG_DIR=/opt"
    ports:
      - "8888:80"
    volumes:
      - "/root/web/index.html:/var/www/html/index.html"

  db:
    image: mysql:8
    environment:
      - "MYSQL_ROOT_PASSWORD=forma"
    ports:
      - "127.0.0.1:3306:3306"
```

> Comme pour le `Dockerfile`, la **[référence de la documentation officielle](https://docs.docker.com/compose/compose-file/)** est très claire et complète pour le contenu des fichiers `docker-compose.yml`.

Afin de tester le fichier, et lancer les conteneurs :

```bash
# Depuis le dossier où se trouve le fichier docker-compose.yml
$ docker-compose up -d

# On peut voir les conteneurs en cours d'exécution
$ docker ps
$ docker-compose ps
```

## 2. Volumes et Networks

Deuxième `docker-compose.yml`, qui met en place l'utilisation des clauses `networks` et `volumes` : 
```yaml
version: "3"

services:
  web:
    image: httpd
    environment:
      - "APACHE_LOG_DIR=/opt"
    ports:
      - "8888:80"
    volumes:
      - "/root/web/index.html:/var/www/html/index.html"
      - test-forma-volume-1:/opt/test-forma-volume-1
    networks:
      test-forma:
        aliases:
          - "web"
          - "web.test"

  db:
    image: mysql:8
    environment:
      - "MYSQL_ROOT_PASSWORD=forma"
    ports:
      - "127.0.0.1:3306:3306"
    networks:
      test-forma:
        aliases:
          - "db"
          - "db.test"
      autre_reseau:

volumes:
  test-forma-volume-1:
  test-forma-volume-2:
  test-forma-volume-3:
  test-forma-volume-4:

networks:
  test-forma:
```

## 3. Wordpress + Reverse proxy

Rédaction d'un `docker-compose.yml` pour Wordpress, et ajout d'un reverse proxy NGINX en frontal : 

```yaml
version: '3.3'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     environment:
       MYSQL_ROOT_PASSWORD: somewp
       MYSQL_DATABASE: wp
       MYSQL_USER: wp
       MYSQL_PASSWORD: wp
     networks:
       wp:
         aliases:
           - "db"
           - "db.forma"

   wp:
     image: wordpress:latest
     depends_on:
       - db
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wp
       WORDPRESS_DB_PASSWORD: wp
       WORDPRESS_DB_NAME: wp
     networks:
       wp:
         aliases:
           - "wp"
           - "wp.forma"
       front:
         aliases:
           - "wp"
           - "wp.front"

   rp:
     image: nginx:1.19
     depends_on:
       - wp
     ports: 
       - "8888:8888"
     volumes:
       - "./wp.conf:/etc/nginx/conf.d/wp.conf"
     networks:
       front:
         aliases:
           - "rp"
           - "rp.front"
        

volumes:
  db_data: {}

networks:
  wp:
  front:
```

Le fichier de configuration NGINX `wp.conf` associé : 
```conf
server {
    server_name   forma.test;
    listen        8888;

    location      / {
	    proxy_pass http://wp.front;
    }
}
```

> Dans la configuration du reverse proxy NGINX, notez l'utilisation du nom de domaine `wp.front` pour faire référence au conteneur Wordpress. Cela est permis grâce aux `aliases` présents dans le fichier `docker-compose.yml`.
