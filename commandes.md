# Mémo commandes

Vous trouverez ici un mémo des commandes `docker` récurrentes, ainsi que des options les plus utilisées.

Globalement, pour interagir avec des conteneur, la ligne de commandes Docker est standard. La structure d'une ligne de commande est la suivante : 

```bash
$ docker <ENTITE> <ACTION>
```

Les entités sont par exemple `image`, `container` ou encore `volume`. 

Les actions sont `ls` (pour lister) ou encore `rm` (pour supprimer).

---

<!-- vim-markdown-toc GitLab -->

* [Lister les images](#lister-les-images)
* [Récupérer une image](#récupérer-une-image)
* [Lancer un conteneur](#lancer-un-conteneur)
    * [Options récurrentes](#options-récurrentes)
* [Volumes Docker](#volumes-docker)
* [Lister les conteneurs](#lister-les-conteneurs)
* [Supprimer un conteneur](#supprimer-un-conteneur)
* [Inspecter un conteneur](#inspecter-un-conteneur)
* [Exécuter une commande dans un conteneur](#exécuter-une-commande-dans-un-conteneur)
* [Déplacer des fichiers entre hôte et conteneurs](#déplacer-des-fichiers-entre-hôte-et-conteneurs)

<!-- vim-markdown-toc -->

## Lister les images

```bash
# Façon historique, rapide
$ docker images
# Façon standard
$ docker image list
```

## Récupérer une image

Commande `docker pull`.

```bash
$ docker pull <IMAGE>

# Pour récupérer une image debian depuis le Docker Hub
$ docker pull debian

# On peut préciser un numéro de version spécifique
$ docker pull debian:latest # la version "latest" est implicite si on ne précise aucune version
$ docker pull debian:8
```

## Lancer un conteneur

Commande `docker run`. 

```bash
# Lancer un conteneur debian
# Si l'image n'existe pas, elle sera pull automatiquement depuis le Docker Hub
$ docker run debian

# Lancer un conteneur debian qui exécute un sleep 10000
$ docker run debian sleep 10000

# Lancer un conteneur debian qui exécute un sleep 10000, en tâche de fond
$ docker run -d debian sleep 10000
```

> **NB :** les options comme `-d` doivent être précisée après l'action (ici `docker run`) et avant les arguments (le nom de l'image `debian`, et la commande `sleep 10000`).

### Options récurrentes

| Option | Fonction | Exemple |
| --- | --- | --- |
| `-d` | Lancer le conteneur en démon, en tâche de fond | `docker run -d debian sleep 9999` |
| `-p` | Partager un port de l'hôte vers le conteur | `docker run -p 8888:80 apache` : partage le port 8888 de l'hôte vers le port 80 du conteneur |
| `-it` | Lancer le conteneur en mode interactif (idéal pour y exécuter un terminal) | `docker run -it debian bash` |
| `-v` | Met en place un volume Docker | Voir section dédiée (ci-dessous)|

## Volumes Docker

Les volumes permettent à un conteneur d'utiliser un espace de donné indépendant de son cycle de vie. C'est à dire, cela permet d'avoir des données persistente lors de l'utilisation de conteneurs. 

Il existe plusieurs types de volumes : 
* dossier partagé
  * on indique un chemin de l'hôte que l'on souhaite synchroniser avec un chemin dans le conteneur
  * utilisé pour accéder simplement aux données générées par le conteneur depuis l'hôte
* volume nommé
  * on donne un nom arbitraire à un volume
  * Docker s'occupe de créer un dossier sur l'hôte
  * le dossier est accessible par le conteneur sur un chemin donné
  * ce volume peut être utilisé par d'autres conteneurs à l'aide de son nom
* volume anonyme
  * moins utilisé
  * permet de créer un volume sans nom, à des fins de test
  * là aussi Docker s'occupera de créer le dossier associé sur l'hôte

Exemples : 
```bash
# Dossier partagé
# On synchronise /opt/test de l'hôte avec /opt/host_test du conteneur
$ docker run -d -v /opt/test:/opt/host_test debian

# Volume nommé
$ docker run --name debian_2 -d -v test:/opt/host_test debian
$ docker run --name debian_1 -d -v test:/opt/host_test debian

# Volume anonyme
$ docker run -d -v /opt/host_test debian
```

## Lister les conteneurs

Commande `docker ps`.

```bash
# Lister les conteneurs en cours d'exécution
$ docker ps

# Lister tous les conteneurs y compris ceux arrêtés
$ docker ps -a
```

## Supprimer un conteneur

Commande `docker rm`.

```bash
# On repère le nom ou l'ID d'un conteneur lancé
$ docker ps

# Suppresion d'un conteneur donné
$ docker rm <NOM_OU_ID_CONTENEUR> 
```

## Inspecter un conteneur

Commande `docker inspect`.

```bash
# On repère le nom ou l'ID d'un conteneur lancé
$ docker ps

# Récupérer les informations d'un conteneur donné
$ docker inspect <NOM_OU_ID_CONTENEUR> 
```

## Exécuter une commande dans un conteneur

Commande `docker exec`.

```bash
# On repère le nom ou l'ID d'un conteneur lancé
$ docker ps

# Exécution d'un ls de la racine (dossier /), dans le conteneur
$ docker exec <NOM_OU_ID_CONTENEUR> ls /

# Exécution d'un terminal bash dans le conteneur
$ docker exec -it <NOM_OU_ID_CONTENEUR> bash
```

> **NB :** pour que le `docker exec` fonctionne, il faut que la commande demandée existe dans le conteneur.

## Déplacer des fichiers entre hôte et conteneurs

Commande `docker cp`.

```bash
$ docker cp <SOURCE> <DESTINATION>
```

La source peut être soit l'hôte, soit le conteneur. Idem pour la destination.

```bash
# On considère un conteneur nommé "FORMA"
# On veut récupérer le fichier /opt/FORMA.txt qui se trouve dans le conteneur FORMA
# Le fichier sera copié dans /opt/TEST.txt sur l'hôte
$ docker cp FORMA:/opt/FORMA.txt /opt/TEST.txt
```

```bash
# On considère un conteneur nommé "FORMA2"
# On veut déposer dans le conteneur FORMA2 un fichier /opt/FORMA2.txt
# La source de ce fichier est sur l'hôte, dans le chemin ./TEST2.txt
$ docker cp  ./TEST2.txt FORMA:/opt/FORMA2.txt
```

```bash
# On considère deux conteneurs nommés "FORMA10" et "FORMA20"
# On veut copier le fichier /opt/FORMA10.txt qui se trouve dans le conteneur FORMA10
# Et le coller vers le fichier /etc/FORMA20.txt dans le conteneur FORMA20
$ docker cp  FORMA10:/opt/FORMA10.txt FORMA20:/etc/FORMA20.txt
```

